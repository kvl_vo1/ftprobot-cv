package configs

import (
  c "ftprobot-cv/common"

  "bytes"
  "crypto/tls"
  "encoding/base64"
  "encoding/csv"
  "encoding/json"
  "errors"
  "fmt"
  "io/ioutil"
  "net/http"
  "path/filepath"
  "strings"
  "sync"
  "time"

  "golang.org/x/text/encoding/charmap"
)

type header struct {
  Name,
  Value string
}

type headers []header

type column struct {
  Name, // назва колонки в JSON
  ColName, // назва колонки в файлі
  Value string         // значення з прайса
  DefValue interface{} // значення по-замовчуванню
  IsRequired, // не пусте (!= "")
  IsUsing, // використовується в проекті
  IsStored bool // заповнено
}

type columns []column

type projectInfo struct {
  id,
  colsCount int // максимальна кількість колонок які мають бути в прайсі для конкретного проекту
  isActive bool
}

type projectsList = map[string]projectInfo

type priceMeta struct {
  HTag string   `json:"htag,omitempty"`
  Span []string `json:"span,omitempty"`
  Nick string   `json:"nick,omitempty"`
  CTag string   `json:"ctag,omitempty"`
}

type itemSale struct {
  ID        string  `json:"id,omitempty"`
  Name      string  `json:"name,omitempty"`
  Stock     float64 `json:"stock,omitempty"`
  QuantIn   float64 `json:"quant_in,omitempty"`
  PriceIn   float64 `json:"price_in,omitempty"`
  QuantOut  float64 `json:"quant_out,omitempty"`
  PriceOut  float64 `json:"price_out,omitempty"`
  SuppName  string  `json:"supp_name,omitempty"`
  SuppCode  string  `json:"supp_code,omitempty"`
  Reimburse bool    `json:"reimburse,omitempty"`
}

type priceSale struct {
  Meta priceMeta  `json:"meta"`
  Data []itemSale `json:"data"`
}

type csvRows [][]string

type skynet struct {
  htag            string
  name            string
  fromMailBox     string
  recipErrorFiles []string
  recipProtocols  []string
  projects        projectsList
  cols            columns
  headers         headers
  log             func(msg string)
  Key             string `json:"key" toml:"key"`
  Url             string `json:"url" toml:"url"`
  NeedToGZip      bool   `json:"needToGZip" toml:"needToGZip"`
}

func newSkynet(logFunc func(msg string)) *skynet {
  w := skynet{}

  w.projects = projectsList{
    "price.period1.country": {colsCount: 11, isActive: false, id: 1},
    "price.period2.country":  {colsCount: 11, isActive: false, id: 2},
    "price.period3.country":   {colsCount: 11, isActive: false, id: 4},
  }

  w.cols = make([]column, 12)
  w.cols[0] = column{Name: "id", ColName: "EXTERNAL_CODE"}
  w.cols[1] = column{Name: "name", ColName: "DRUG_NAME"}
  w.cols[2] = column{Name: "stock", ColName: "BALANCE_BEGIN"}
  w.cols[3] = column{Name: "qcountrynt_in", ColName: "QUANTITY_CREDIT"}
  w.cols[4] = column{Name: "price_in", ColName: "PRICE_CREDIT"}
  w.cols[5] = column{Name: "qcountrynt_out", ColName: "QUANTITY_DEBIT"}
  w.cols[6] = column{Name: "price_out", ColName: "PRICE_DEBIT"}
  w.cols[7] = column{Name: "span", ColName: "DATE_BEGIN"}
  w.cols[8] = column{Name: "span", ColName: "DATE_END"}
  w.cols[9] = column{Name: "supp_name", ColName: "SUPPLIER_NAME"}
  w.cols[10] = column{Name: "supp_code", ColName: "CODE_OKPO"}
  w.cols[11] = column{Name: "reimburse", ColName: "IS_REIMB"}

  w.log = logFunc

  return &w
}

func (w *skynet) Init(name, project, fromMailBox string, re, rp []string) error {
  w.name = name
  w.htag = project
  w.fromMailBox = fromMailBox
  w.recipErrorFiles = re
  w.recipProtocols = rp

  if !c.IsIncluded([]string{project}, w.htag) || !w.projects[project].isActive {
    return errors.New(fmt.Sprintf(`Проект "%s" не знайдено або він заблокований!`, project))
  }

  if w.projects[project].colsCount <= 0 {
    return errors.New(fmt.Sprintf(`В проекті "%s" недопустима кількість колонок(%d)!`, project, w.projects[project].colsCount))
  }

  if w.projects[project].colsCount > len(w.cols) {
    return errors.New(fmt.Sprintf(`В проекті "%s" кількість колонок(%d) не повинна перевищувати загальну кількість(%d)!`, project, w.projects[project].colsCount, len(w.cols)))
  }

  for i := range w.cols {
    // перших 9 полів завжди обовязкові (окрім другого)
    w.cols[i].IsRequired = i <= 8

    // поле "name" може бути пустим
    switch i {
    case 1:
      w.cols[i].IsRequired = false
    }

    // значення по-замовчуванню
    switch i {
    case 0, 1:
      w.cols[i].DefValue = ""
    case 2, 3, 4, 5, 6:
      w.cols[i].DefValue = 0
    default:
      w.cols[i].DefValue = nil
    }

    switch i {
    // id, name використовуються у всіх проектах
    case 0, 1:
      w.cols[i].IsUsing = true

    case 2, 3, 4, 7, 8:
      w.cols[i].IsUsing = !c.IsIncluded([]string{"price4.period.country", "price4.period.country"}, w.htag)

    case 5, 6:
      w.cols[i].IsUsing = !c.IsIncluded([]string{"price.period.country", "price.period2.country", "price.period3.country"}, w.htag)

    case 9, 10:
      w.cols[i].IsUsing = c.IsIncluded([]string{"price.period.country", "price.period2.country", "price.period3.country", "price.period3.country", "price.period.country"}, w.htag)

    case 11:
      w.cols[i].IsUsing = c.IsIncluded([]string{"price.period3.country", "price.period2.country"}, w.htag)
    }
  }

  w.headers = make(headers, 3, 10)
  w.headers = []header{
    {"content-type", "application/json; charset=utf-8"},
    {"User-Agent", "ftpRobotPriceParser"},
    {"Authorization", `Basic ` + c.Base64Encode(fmt.Sprintf(`api:key-%s`, w.Key))},
  }
  if w.NeedToGZip {
    w.headers = append(w.headers, header{Name: "Content-Encoding", Value: "gzip"})
  }

  return nil
}

func (w *skynet) fillDates(span *[]string, dateStart, dateFinish string) error {
  const dateMask = "02.01.2006"
  dateStart = c.PrepareDate(dateStart)
  dateFinish = c.PrepareDate(dateFinish)

  t, err := time.Parse(dateMask, dateStart)
  if err != nil {
    return errors.New(fmt.Sprintf(`"%s": %s`, w.cols[7].ColName, err.Error()))
  }
  (*span)[0] = t.Format(dateMask) + " 00:00:00"

  t, err = time.Parse(dateMask, dateFinish)
  if err != nil {
    return errors.New(fmt.Sprintf(`"%s": %s`, w.cols[8].ColName, err.Error()))
  }
  (*span)[1] = t.Format(dateMask) + " 00:00:00"

  return nil
}

func (w *skynet) parseCSV(fn string, rec *csvRows) error {
  data, err := ioutil.ReadFile(fn)
  if err != nil {
    return err
  }

  dec := charmap.Windows1251.NewDecoder()
  out, err := dec.Bytes(data)
  if err != nil {
    return err
  }

  r := csv.NewReader(bytes.NewReader(out))
  r.Comma = '\t'
  r.Comment = '#'
  r.LazyQuotes = true

  *rec, err = r.ReadAll()
  if err != nil {
    return err
  }

  return nil
}

func (w *skynet) fillJson(price priceSale, rows csvRows) error {
  const wrongColData = `помилка в рядку %d колонці "%s": %s`

  var isDateFilled bool
  var err error

  // цикл по рядках в прайсі
  for j := range rows {
    // якщо в самому першому рядку значення першого по-порядку поля рівне "EXTERNAL_CODE" - вважаю що це заголовки
    if strings.ToUpper(rows[0][0]) == w.cols[0].ColName {
      continue
    }

    // цикл по колонках в рядку
    for k := 0; k < (w.projects[w.htag].colsCount); k++ {
      // скасування ознаки "заповнено"
      w.cols[k].Value = ""
      w.cols[k].IsStored = false

      // кількість колонок в прайсі може бути більшою ніж загальна кількість всіх колонок
      if k < len(rows[j]) {
        w.cols[k].Value = rows[j][k]
        w.cols[k].IsStored = w.cols[k].Value != ""
      }

      // проставлення дефолтних значень
      if w.cols[k].IsRequired && w.cols[k].IsUsing && !w.cols[k].IsStored && (w.cols[k].DefValue != nil) {
        w.cols[k].Value, err = c.GetString(w.cols[k].DefValue)
        if err != nil {
          return errors.New(fmt.Sprintf(wrongColData, j+1, w.cols[k].ColName, err.Error()))
        }
        w.cols[k].IsStored = true
      }

      if w.cols[k].IsRequired && w.cols[k].IsUsing && !w.cols[k].IsStored {
        return errors.New(fmt.Sprintf(`в рядку %d колонка "%s" обов''язкова до заповнення`, j+1, w.cols[k].ColName))
      }

      // Заповнення JSONа даними
      if w.cols[k].IsUsing && w.cols[k].IsStored {
        switch k {
        case 0:
          price.Data[j].ID = c.ReplaceRepeatableStrByOneStr(w.cols[k].Value, " ", " ")

        case 1:
          price.Data[j].Name = c.ReplaceRepeatableStrByOneStr(w.cols[k].Value, " ", " ")

        case 2:
          price.Data[j].Stock, err = c.ToFloat64(w.cols[k].Value)
          if err != nil {
            return errors.New(fmt.Sprintf(wrongColData, j+1, w.cols[k].ColName, err.Error()))
          }

        case 3:
          price.Data[j].QuantIn, err = c.ToFloat64(w.cols[k].Value)
          if err != nil {
            return errors.New(fmt.Sprintf(wrongColData, j+1, w.cols[k].ColName, err.Error()))
          }

        case 4:
          price.Data[j].PriceIn, err = c.ToFloat64(w.cols[k].Value)
          if err != nil {
            return errors.New(fmt.Sprintf(wrongColData, j+1, w.cols[k].ColName, err.Error()))
          }

        case 5:
          price.Data[j].QuantOut, err = c.ToFloat64(w.cols[k].Value)
          if err != nil {
            return errors.New(fmt.Sprintf(wrongColData, j+1, w.cols[k].ColName, err.Error()))
          }

        case 6:
          price.Data[j].PriceOut, err = c.ToFloat64(w.cols[k].Value)
          if err != nil {
            return errors.New(fmt.Sprintf(wrongColData, j+1, w.cols[k].ColName, err.Error()))
          }

        case /*7,*/ 8:
          if !isDateFilled {
            if e := w.fillDates(&price.Meta.Span, w.cols[7].Value, w.cols[8].Value); e != nil {
              return errors.New(fmt.Sprintf(wrongColData, j+1, fmt.Sprintf(`%s(%s)`, w.cols[7].ColName, w.cols[k].ColName), e.Error()))
            }

            isDateFilled = true
          }

        case 9:
          price.Data[j].SuppName = strings.TrimSpace(w.cols[k].Value)

        case 10:
          price.Data[j].SuppCode = strings.TrimSpace(w.cols[k].Value)

        case 11:
          reimb := strings.ToLower(w.cols[k].Value)
          price.Data[j].Reimburse = reimb == "true" || reimb == "1"
        }
      }
    }
  }

  return nil
}

func (w *skynet) postFile(h headers, data []byte) (int, error) {
  isGzip := false
  for x := range h {
    if h[x].Name == "Content-Encoding" && strings.Contains(h[x].Value, "gzip") {
      isGzip = true
      break
    }
  }

  var buf bytes.Buffer

  if isGzip {
    b, err := c.GZip(data)
    if err != nil {
      return -1, err
    }

    _, err = buf.Write(b)
    if err != nil {
      return -1, err
    }

  } else {
    _, err := buf.Write(data)
    if err != nil {
      return -1, err
    }
  }

  tr := &http.Transport{
    TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
  }
  client := &http.Client{Transport: tr}

  req, err := http.NewRequest("POST", w.Url, &buf)
  if err != nil {
    return -1, err
  }

  // заповнюю хедери
  for i := range h {
    req.Header.Add(h[i].Name, h[i].Value)
  }

  resp, err := client.Do(req)
  if err != nil {
    return -1, err
  }
  defer func() {
    _ = resp.Body.Close()
  }()

  return resp.StatusCode, nil
}

func (w *skynet) ParseAndPost(files []c.File) error {
  var mu sync.Mutex
  var wg sync.WaitGroup

  var jsonMaker = func(file *c.File) error {
    if !file.HasError {
      var rows csvRows
      var price priceSale

      err := w.parseCSV(file.Name, &rows)
      if err != nil {
        file.HasError = true
        file.Error = err.Error()
        return err
      }

      if len(rows) == 0 {
        file.HasError = true
        file.Error = "Файл пустий"
        return errors.New(file.Error)
      }

      defer mu.Unlock()
      mu.Lock()
      price = priceSale{}
      price.Meta.HTag = w.htag
      price.Meta.Nick = filepath.Base(c.FilenameWithoutExtension(file.Name))
      price.Meta.Span = make([]string, 2)

      if len(w.recipErrorFiles) > 0 || len(w.recipProtocols) > 0 {
        ctag := newSpi(w.name, filepath.Base(file.Name), w.fromMailBox, w.recipErrorFiles, w.recipProtocols, file.Size)
        price.Meta.CTag, err = ctag.makeBase64()
        if err != nil {
          return err
        }
      }

      price.Data = make([]itemSale, len(rows))

      if e := w.fillJson(price, rows); e != nil {
        file.HasError = true
        file.Error = e.Error()
        return e
      } else {
        file.Meta, _ = json.Marshal(price.Meta)
        file.Data, _ = json.Marshal(price.Data)
      }

      return nil
    }

    return errors.New(file.Error)
  }

  var jsonSender = func(file *c.File) error {
    if !file.HasError {
      h := make(headers, len(w.headers))
      copy(h, w.headers)

      if file.Meta != nil {
        h = append(h, header{"Content-Meta", base64.StdEncoding.EncodeToString(file.Meta)})
      }

      var err error
      file.Status, err = w.postFile(h, file.Data)
      if err != nil {
        file.HasError = true
        file.Error = fmt.Sprintf("[%d] %s", file.Status, err.Error())
        return err
      }
    }

    return nil
  }

  var workersCount = c.IifInt(len(files) < 16, len(files), 16)
  cj := make(chan *c.File, workersCount)

  for i := 0; i < workersCount; i++ {
    wg.Add(1)

    go func(in <-chan *c.File) {
      defer wg.Done()

      for {
        file, ok := <-in
        if !ok {
          break
        }

        // парсинг даних в JSON
        w.log(fmt.Sprintf(`[make] %s`, file.Name))
        if err := jsonMaker(file); err != nil {
          w.log(fmt.Sprintf("[make] %s: %s", file.Name, err.Error()))
          continue
        }

        //відправлення JSON в сервіс
        w.log(fmt.Sprintf(fmt.Sprintf(`[post] %s (%d)`, file.Name, len(file.Data))))
        if err := jsonSender(file); err != nil {
          w.log(fmt.Sprintf("[post] %s: %s", file.Name, err.Error()))
          continue
        }
      }
    }(cj)
  }

  // запуск всіх процесів
  w.log(fmt.Sprintf("Кількість потоків для обробки: %d", workersCount))
  for i := range files {
    cj <- &files[i]
  }
  close(cj)
  wg.Wait()

  return nil
}
