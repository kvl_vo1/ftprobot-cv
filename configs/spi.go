package configs

import (
  "encoding/base64"
  "encoding/json"
  "fmt"
  "strconv"
  "strings"
)

type Spi struct {
  Sender              string `json:"Sender"`
  ReceivedFromMailBox string `json:"ReceivedFromMailBox"`
  FromAddress         string `json:"FromAddress"`
  SubjectOriginal     string `json:"SubjectOriginal"`
  Subject             string `json:"Subject"`
  DateFromSubject     string `json:"DateFromSubject"`
  FileName            string `json:"FileName"`
  FileSize            string `json:"FileSize"`
  Body                string `json:"Body"`
  RecipErrorFiles     string `json:"RecipErrorFiles"` // електропошта тих хто отримуватиме помилкові файли
  RecipProtocols      string `json:"RecipProtocols"`  // електропошта тих хто отримуватиме логи помилок
}

func newSpi(project, fileName, fromMailBox string, re, rp []string, fileSize uint64) *Spi {
  s := Spi{
    Sender:              "SPI",
    ReceivedFromMailBox: fromMailBox,
    FromAddress:         fromMailBox,
    SubjectOriginal:     project,
    Subject:             fmt.Sprintf(`Помилка в файлі "%s"`, fileName),
    FileName:            fileName,
    FileSize:            strconv.FormatUint(fileSize, 10),
    Body:                fmt.Sprintf("Поточний проект: %s\n------------------------------------------------\nФайл \"%s\" не опрацьований. Помилка: %s", project, fileName, "%s"),
    RecipErrorFiles:     strings.Join(re, "\n"),
    RecipProtocols:      strings.Join(rp, "\n"),
  }

  return &s
}

func (s *Spi) makeBase64() (string, error) {
  b, err := json.MarshalIndent(s, "", "  ")
  if err != nil {
    return "", err
  }

  return "data:application/json;base64," + base64.StdEncoding.EncodeToString(b), nil
}
