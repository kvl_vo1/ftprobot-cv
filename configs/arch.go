package configs

type archiverConfig struct {
  Description       string   `json:"description" toml:"description"`
  UseInternal       bool     `json:"useInternal" toml:"useInternal"`
  Path              string   `json:"path" toml:"path"`
  Args              string   `json:"args" toml:"args"`
  ExtList           []string `json:"extList" toml:"extList"`
  DeleteAfterUnpack bool     `json:"deleteAfterUnpack" toml:"deleteAfterUnpack"`
}
