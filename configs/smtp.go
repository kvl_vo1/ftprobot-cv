package configs

import "fmt"

type smtpConfig struct {
  Server              string `json:"server" toml:"server"`
  Port                int    `json:"port" toml:"port"`
  Login               string `json:"login" toml:"login"`
  Password            string `json:"password" toml:"password"`
  From                string `json:"from" toml:"from"`
  NeedToZip           bool   `json:"needToZip" toml:"needToZip"`
  MaxAttachmentSizeMB int    `json:"maxAttachmentSizeMB" toml:"maxAttachmentSizeMB"`
}

func (s *smtpConfig) Addr() string {
  return fmt.Sprintf("%s:%d", s.Server, s.Port)
}
