package configs

import (
  cm "ftprobot-cv/common"
  cr "ftprobot-cv/crypt"

  "bytes"
  "errors"
  "flag"
  "fmt"
  "io/ioutil"
  "log"
  "os"
  "path/filepath"
  "strings"
  "time"

  "github.com/BurntSushi/toml"
)

type Config struct {
  logger         *log.Logger
  timestamp      string
  newPrefix      string
  crypt          *cr.Crypter
  IsManual       bool           `json:"-" toml:"-"`
  ManualFiles    string         `json:"-" toml:"-"`
  FileLogName    string         `json:"-" toml:"-"`
  RootDir        string         `json:"-" toml:"-"`
  DataDir        string         `json:"-" toml:"-"`
  Project        cm.Project     `json:"project" toml:"project"`
  Skynet         *skynet        `json:"skynet" toml:"skynet"`
  FtpConfig      ftpConfig      `json:"ftp" toml:"ftp"`
  SmtpConfig     smtpConfig     `json:"smtp" toml:"smtp"`
  ArchiverConfig archiverConfig `json:"archiver" toml:"archiver"`
}

func (c *Config) Init() error {
  flag.StringVar(&c.ManualFiles, "files", "", "Файл з прайсами")
  flag.Parse()
  c.IsManual = cm.FileExists(c.ManualFiles)

  c.newPrefix = "new:"
  c.timestamp = time.Now().Format("060102.150405.00")

  c.RootDir = filepath.Dir(os.Args[0])
  c.DataDir = filepath.Join(c.RootDir, filepath.Join("data", c.timestamp, "files"))

  c.Project.Name = "PriceProject#001"
  c.Project.Htag = "price.daily.001"
  c.Project.ExtList = []string{".pdd"}

  c.Project.UseMail = false
  c.Project.MailTo = []string{"recipient@mail.ua"}
  c.Project.LogTo = []string{"robot-logs@robots.mail.ua"}
  c.Project.LogCC = []string{}
  c.Project.LogBCC = []string{}

  c.Skynet = newSkynet(c.Log)
  c.Skynet.Key = fmt.Sprintf("%sa48e5f277361d826e482fe84f8421fbea9014f27", c.newPrefix)
  c.Skynet.Url = "https://price.service.ua/stream-put-data"
  c.Skynet.NeedToGZip = true

  c.FtpConfig.Server = "localhost"
  c.FtpConfig.Port = 21
  c.FtpConfig.Login = "ftprobot"
  c.FtpConfig.Password = fmt.Sprintf("%spassword", c.newPrefix)
  c.FtpConfig.Dir = "/"
  c.FtpConfig.ExtList = []string{".rar", ".zip", ".7z", ".mdr"}
  c.FtpConfig.DelAfterReceive = false

  c.SmtpConfig.Server = "robots.morion.ua"
  c.SmtpConfig.Port = 465
  c.SmtpConfig.Login = "robot-logs@robots.mail.ua"
  c.SmtpConfig.Password = fmt.Sprintf("%seXuqrWJifYBSOKHJfvIF", c.newPrefix)
  c.SmtpConfig.From = "robot-logs@robots.mail.ua"
  c.SmtpConfig.NeedToZip = true
  c.SmtpConfig.MaxAttachmentSizeMB = 32

  c.ArchiverConfig.Description = "Arguments for: #[7zip]: x %archive_name% -o%directory% #[WinRar]: x -ibck %archive_name% %directory%"
  c.ArchiverConfig.UseInternal = false
  c.ArchiverConfig.Path = `c:\Program Files (x86)\7-Zip\7z.exe`
  c.ArchiverConfig.Args = "x %archive_name% -o%directory%"
  c.ArchiverConfig.ExtList = []string{".rar", ".zip", ".7z"}
  c.ArchiverConfig.DeleteAfterUnpack = false

  c.crypt = cr.New()

  // making Data directory
  if !cm.FileExists(c.DataDir) {
    if e := os.MkdirAll(c.DataDir, os.ModePerm); e != nil {
      return e
    }
  }

  // making log file
  c.FileLogName = filepath.Join(c.DataDir, "..", fmt.Sprintf("log_%s.txt", c.timestamp))
  fileLog, err := os.OpenFile(c.FileLogName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
  if err != nil {
    return errors.New(fmt.Sprintf("Помилка створення [%s] (%s)", "log.txt", err))
  }
  c.logger = log.New(fileLog, "", log.LstdFlags)

  if c.ManualFiles != "" && !c.IsManual {
    return errors.New(fmt.Sprintf("Файл \"%s\" не знайдено!", c.ManualFiles))
  }

  return nil
}

// encDec
// Шифрування і розшифровування пароля.
// pwd string: пароль. Може починатися з "new:". Якщо це так - значить задається новий.
// encryptOnly bool: true - тільки шифрує пароль, false - розшифровує його.
func (c *Config) encDec(pwd string, encryptOnly bool) (string, error) {
  if pwd == "" || pwd == c.newPrefix {
    return "", nil
  }

  if strings.HasPrefix(pwd, c.newPrefix) {
    p := pwd[len(c.newPrefix):]
    return c.crypt.Encrypt(p)
  } else {
    if encryptOnly {
      return pwd, nil
    }
    return c.crypt.Decrypt(pwd)
  }
}

func (c *Config) LoadConfig() error {
  fn := "settings.toml"
  sf := filepath.Join(c.RootDir, fn)

  var tomlToBytes = func(v interface{}) ([]byte, error) {
    b := new(bytes.Buffer)
    if e := toml.NewEncoder(b).Encode(v); e != nil {
      return nil, e
    }
    return b.Bytes(), nil
  }

  if !cm.FileExists(sf) {
    b, err := tomlToBytes(*c)
    if err != nil {
      return err
    }

    // saving current params to a file
    if e := ioutil.WriteFile(sf, b, os.ModePerm); e != nil {
      return e
    }

    fmt.Println("Ініціалізація програми.")
    fmt.Println(fmt.Sprintf("Створений конфігураційний файл [%s].", fn))
    fmt.Println("Первірте налаштування.")
    fmt.Println("Натисність будь-яку клавішу для продовження.")
    var input string
    _, _ = fmt.Scanln(&input)
    return errors.New("Перший запуск!")
  } else {
    b, e := ioutil.ReadFile(sf)
    if e != nil {
      return e
    }

    if e = toml.Unmarshal(b, &(c)); e != nil {
      return e
    }

    var removeEmptyItems = func(a []string) []string {
      s := make([]string, 0, len(a))
      for _, item := range a {
        if strings.TrimSpace(item) != "" {
          s = append(s, item)
        }
      }
      return s
    }
    c.Project.MailTo = removeEmptyItems(c.Project.MailTo)
    c.Project.LogTo = removeEmptyItems(c.Project.LogTo)
    c.Project.LogCC = removeEmptyItems(c.Project.LogCC)
    c.Project.LogBCC = removeEmptyItems(c.Project.LogBCC)

    needToSave :=
      strings.HasPrefix(c.FtpConfig.Password, c.newPrefix) ||
        strings.HasPrefix(c.SmtpConfig.Password, c.newPrefix) ||
        strings.HasPrefix(c.Skynet.Key, c.newPrefix)

    // Максимальний об'єм приєднання = 32МБ
    if c.SmtpConfig.NeedToZip && ((c.SmtpConfig.MaxAttachmentSizeMB <= 0) || (c.SmtpConfig.MaxAttachmentSizeMB > 32)) {
      c.SmtpConfig.MaxAttachmentSizeMB = 32
      needToSave = true
    }

    pwd, e := c.encDec(c.Skynet.Key, true)
    if e != nil {
      return errors.New(fmt.Sprintf(`помилка в "service.key": %s`, e.Error()))
    }
    c.Skynet.Key = pwd

    pwd, e = c.encDec(c.FtpConfig.Password, true)
    if e != nil {
      return errors.New(fmt.Sprintf(`помилка в "ftp.password": %s`, e.Error()))
    }
    c.FtpConfig.Password = pwd

    pwd, e = c.encDec(c.SmtpConfig.Password, true)
    if e != nil {
      return errors.New(fmt.Sprintf(`помилка в "smtp.password": %s`, e.Error()))
    }
    c.SmtpConfig.Password = pwd

    if needToSave {
      tb, err := tomlToBytes(*c)
      if err != nil {
        return err
      }
      if ewf := ioutil.WriteFile(sf, tb, os.ModePerm); ewf != nil {
        return errors.New(fmt.Sprintf(`помилка збереження налаштувань в "%s": %s`, sf, ewf.Error()))
      }
    }

    // розшифровування паролів
    c.Skynet.Key, _ = c.encDec(c.Skynet.Key, false)
    c.FtpConfig.Password, _ = c.encDec(c.FtpConfig.Password, false)
    c.SmtpConfig.Password, _ = c.encDec(c.SmtpConfig.Password, false)

    if ei := c.Skynet.Init(c.Project.Name, c.Project.Htag, c.SmtpConfig.From, c.Project.LogTo, c.Project.LogTo); ei != nil {
      c.Log(ei.Error())
      return ei
    }

    return nil
  }
}

func (c *Config) Log(msg string) {
  if c.logger != nil {
    c.logger.Println(msg)
  } else {
    fmt.Println(msg)
  }
}
