package configs

import "fmt"

type ftpConfig struct {
  Server          string   `json:"server" toml:"server"`
  Port            int      `json:"port" toml:"port"`
  Login           string   `json:"login" toml:"login"`
  Password        string   `json:"password" toml:"password"`
  Dir             string   `json:"dir" toml:"dir"`
  ExtList         []string `json:"extList" toml:"extList"`
  DelAfterReceive bool     `json:"delAfterReceive" toml:"delAfterReceive"`
}

func (s *ftpConfig) Addr() string {
  return fmt.Sprintf("%s:%d", s.Server, s.Port)
}
