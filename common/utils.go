package common

import (
  "bytes"
  "compress/gzip"
  "crypto/rand"
  "encoding/base64"
  "errors"
  "fmt"
  "io/ioutil"
  "log"
  "math"
  "os"
  "path"
  "reflect"
  "regexp"
  "strconv"
  "strings"
)

func IifString(b bool, isTrue, isFalse string) string {
  if b {
    return isTrue
  } else {
    return isFalse
  }
}

func IifInt(b bool, isTrue, isFalse int) int {
  if b {
    return isTrue
  } else {
    return isFalse
  }
}

func MakeFakeUUID() (string, error) {
  b := make([]byte, 16)
  _, err := rand.Read(b)
  if err != nil {
    return "", err
  }
  return fmt.Sprintf("%x-%x-%x-%x-%x", b[0:4], b[4:6], b[6:8], b[8:10], b[10:]), nil
}

func FilenameWithoutExtension(filePath string) string {
  return strings.TrimSuffix(filePath, path.Ext(filePath))
}

func ChangeFilenameExtension(filePath, newExt string) string {
  return FilenameWithoutExtension(filePath) + newExt
}

func ReplaceRepeatableStrByOneStr(srcStr, replaceWhat, replaceBy string) string {
  re := regexp.MustCompile(fmt.Sprintf(`(%s){2,}`, replaceWhat))
  return string(re.ReplaceAll([]byte(srcStr), []byte(replaceBy)))
}

func prepareNumber(s string) string {
  var res = strings.TrimSpace(s)
  res = strings.Replace(res, ",", ".", -1)
  res = strings.Replace(res, " ", "", -1)
  res = strings.Replace(res, string([]byte{160}), "", -1)
  return res
}

func PrepareDate(s string) string {
  var res = strings.TrimSpace(s)
  res = strings.Replace(res, ",", ".", -1)
  res = strings.Replace(res, "/", ".", -1)
  res = strings.Replace(res, "-", ".", -1)
  res = strings.Replace(res, string([]byte{160}), "", -1)
  res = ReplaceRepeatableStrByOneStr(res, " ", "")

  return res
}

func ToFloat64(numStr string) (float64, error) {
  return strconv.ParseFloat(prepareNumber(numStr), 64)
}

func ToInt64(numStr string) (int64, error) {
  return strconv.ParseInt(prepareNumber(numStr), 10, 64)
}

func FileExists(filePath string) bool {
  _, err := os.Stat(filePath)
  if err != nil {
    if os.IsNotExist(err) {
      return false
    }
  }
  return true
}

func SaveToFile(data *[]byte, fn string) error {
  file, err := os.Create(fn)
  if err != nil {
    return errors.New(fmt.Sprintf("Помилка при відкритті файла [%s]: %s", fn, err))
  }
  defer func() {
    _ = file.Close()
  }()

  _, err = file.Write(*data)
  if err != nil {
    return err
  }

  return nil
}

func LoadFromFile(fn string) ([]byte, error) {
  f, err := os.Open(fn)
  if err != nil {
    return nil, err
  }
  defer func() {
    _ = f.Close()
  }()

  b, err := ioutil.ReadAll(f)
  if err != nil {
    return nil, err
  }

  return b, nil
}

func GetFloat(unk interface{}) (float64, error) {
  var floatType = reflect.TypeOf(float64(0))
  var stringType = reflect.TypeOf("")

  switch i := unk.(type) {
  case float64:
    return i, nil
  case float32:
    return float64(i), nil
  case int64:
    return float64(i), nil
  case int32:
    return float64(i), nil
  case int:
    return float64(i), nil
  case uint64:
    return float64(i), nil
  case uint32:
    return float64(i), nil
  case uint:
    return float64(i), nil
  case string:
    return strconv.ParseFloat(i, 64)
  default:
    v := reflect.ValueOf(unk)
    v = reflect.Indirect(v)
    if v.Type().ConvertibleTo(floatType) {
      fv := v.Convert(floatType)
      return fv.Float(), nil
    } else if v.Type().ConvertibleTo(stringType) {
      sv := v.Convert(stringType)
      s := sv.String()
      return strconv.ParseFloat(s, 64)
    } else {
      return math.NaN(), fmt.Errorf("Can't convert %v to float64", v.Type())
    }
  }
}

func GetString(unk interface{}) (string, error) {
  var stringType = reflect.TypeOf("")

  switch i := unk.(type) {
  case float64:
    return strconv.FormatFloat(i, 'f', -1, 64), nil
  case float32:
    return strconv.FormatFloat(float64(i), 'f', -1, 32), nil
  case int64:
    return strconv.FormatInt(i, 10), nil
  case int32:
    return strconv.FormatInt(int64(i), 10), nil
  case int16:
    return strconv.FormatInt(int64(i), 10), nil
  case int8:
    return strconv.FormatInt(int64(i), 10), nil
  case int:
    return strconv.FormatInt(int64(i), 10), nil
  case uint64:
    return strconv.FormatInt(int64(i), 10), nil
  case uint32:
    return strconv.FormatInt(int64(i), 10), nil
  case uint:
    return strconv.FormatInt(int64(i), 10), nil
  case string:
    return i, nil
  default:
    v := reflect.ValueOf(unk)
    v = reflect.Indirect(v)
    if v.Type().ConvertibleTo(stringType) {
      sv := v.Convert(stringType)
      return sv.String(), nil
    } else {
      return "", fmt.Errorf("Can't convert %v to string", v.Type())
    }
  }
}

func IsIncluded(pl []string, project string) bool {
  if len(pl) > 0 {
    for index := range pl {
      if pl[index] == project {
        return true
      }
    }
  }

  return false
}

func Base64Encode(s string) string {
  return base64.StdEncoding.EncodeToString([]byte(s))
}

func GZip(data []byte) ([]byte, error) {
  //gzip даних
  var b bytes.Buffer
  w := gzip.NewWriter(&b)
  if _, err := w.Write(data); err != nil {
    return nil, err
  }

  if err := w.Close(); err != nil {
    return nil, err
  }

  return b.Bytes(), nil
}

func UnGZip(data []byte) ([]byte, error) {
  b := bytes.NewReader(data)
  r, err := gzip.NewReader(b)
  if err != nil {
    return nil, err
  }
  defer r.Close()

  gzd, err := ioutil.ReadAll(r)
  if err != nil {
    log.Println(err)
    return nil, err
  }

  return gzd, nil
}
