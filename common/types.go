package common

const (
  _      = iota
  KB int = 1 << (10 * iota)
  MB
  GB
  TB
)

type File struct {
  Id       int    `json:"id"`
  Name     string `json:"name"`
  Size     uint64 `json:"size"`
  HasError bool   `json:"-"`
  Error    string `json:"-"`
  Status   int    `json:"-"`
  Meta     []byte `json:"-"`
  Data     []byte `json:"-"`
}

type Mail struct {
  Subject        string
  Body           string
  AttachmentsIdx []int
  AttachmentSize uint64
  IsSent         bool
  Error          string
}

type Entry struct {
  File
  IsArchive bool
  ArcDir    string
}

type Project struct {
  Name    string   `json:"name" toml:"name"`
  Htag    string   `json:"htag" toml:"htag"`
  ExtList []string `json:"extList" toml:"extList"`
  MailTo  []string `json:"mailTo" toml:"mailTo"` // електропошта проекта, наприклад "project1@mail.ua" або "project2@mail.ua"
  LogTo   []string `json:"logTo" toml:"logTo"`  // електропошта куди відправляти логи і поломані файли
  LogCC   []string `json:"logCC" toml:"logCC"`
  LogBCC  []string `json:"logBCC" toml:"logBCC"`
  UseMail bool     `json:"useMail" toml:"useMail"`
}
