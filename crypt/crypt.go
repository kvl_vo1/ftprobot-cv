package crypt

import (
  "crypto/aes"
  "crypto/cipher"
  "encoding/hex"
)

type Crypter struct {
  key string
  iv  []byte
}

func New() *Crypter {
  c := Crypter{}

  b, _ := hex.DecodeString("433231664d575823215468395e5035454b78634532305a3967557170256f2a74")
  c.key = string(b)

  c.iv, _ = hex.DecodeString("7cd83151e566f8072c4f9a86")

  return &c
}

func (c *Crypter) Encrypt(text string) (string, error) {
  block, err := aes.NewCipher([]byte(c.key))
  if err != nil {
    return "", err
  }
  aesgcm, err := cipher.NewGCM(block)
  if err != nil {
    return "", err
  }
  ciphertext := aesgcm.Seal(nil, c.iv, []byte(text), nil)
  return hex.EncodeToString(ciphertext), nil
}

func (c *Crypter) Decrypt(ct string) (string, error) {
  ciphertext, _ := hex.DecodeString(ct)
  block, err := aes.NewCipher([]byte(c.key))
  if err != nil {
    return "", err
  }
  aesgcm, err := cipher.NewGCM(block)
  if err != nil {
    return "", err
  }
  plaintext, err := aesgcm.Open(nil, c.iv, ciphertext, nil)
  if err != nil {
    return "", err
  }
  s := string(plaintext[:])
  return s, nil
}
