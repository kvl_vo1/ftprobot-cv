package app

import (
  c "ftprobot-cv/common"
  cfg "ftprobot-cv/configs"
  arc "github.com/mholt/archiver"

  "compress/flate"
  "crypto/tls"
  "encoding/json"
  "errors"
  "fmt"
  "io/ioutil"
  "mime"
  "net/smtp"
  "os"
  "os/exec"
  "path/filepath"
  "strconv"
  "strings"
  "sync"
  "time"

  "github.com/jlaffaye/ftp"
  "github.com/jordan-wright/email"
)

type App struct {
  config  cfg.Config
  entries []c.Entry
  files   []c.File
  mails   []c.Mail
}

func (a *App) Init() error {
  if err := a.config.Init(); err != nil {
    return err
  }

  if err := a.config.LoadConfig(); err != nil {
    return err
  }

  a.entries = make([]c.Entry, 0, 10)
  a.files = make([]c.File, 0, 100)
  a.mails = make([]c.Mail, 0, 100)

  return nil
}

func (a *App) Log(msg string) {
  a.config.Log(msg)
}

func (a *App) IsManual() bool {
  return a.config.IsManual
}

func (a *App) LoadFromFTP() error {
  // Connection to FTP
  conf := a.config.FtpConfig

  a.Log(fmt.Sprintf("З'єднання з '%s@%s'...", conf.Login, conf.Addr()))

  // todo перевірка роботи з портом "ftps"
  con, err := ftp.DialTimeout(conf.Addr(), 5*time.Second)
  if err != nil {
    return errors.New(fmt.Sprintf("Помилка з'єднання з %s (%s)", conf.Addr(), err))
  }
  defer func() {
    _ = con.Logout()
    _ = con.Quit()
  }()

  // Login
  if e := con.Login(conf.Login, conf.Password); e != nil {
    return errors.New(fmt.Sprintf("Помилка (Login): %s", e))
  }

  // Change working dir
  if e := con.ChangeDir(conf.Dir); e != nil {
    return errors.New(fmt.Sprintf("Помилка (ChangeDir): %s", e))
  }

  // Files list
  entries, err := con.List(".")
  if err != nil {
    return errors.New(fmt.Sprintf("Помилка (List): %s", err))
  }
  for key := range entries {
    if entries[key].Type == ftp.EntryTypeFile {
      // check for extension
      ext := strings.ToLower(filepath.Ext(entries[key].Name))
      if c.IsIncluded(a.config.FtpConfig.ExtList, ext) {
        isArchive := c.IsIncluded(a.config.ArchiverConfig.ExtList, ext)
        a.entries = append(a.entries, c.Entry{File: c.File{Name: entries[key].Name, Size: entries[key].Size}, IsArchive: isArchive})
      }
    }
  }

  // Цикл по всьому списку найменувань і їх викачування
  if len(a.entries) > 0 {
    a.Log("Отримання файлів:")
    for _, file := range a.entries {
      a.Log(fmt.Sprintf("Отримання [%s] (%d байт)...", file.Name, file.Size))

      r, e := con.Retr(file.Name)
      if e != nil {
        return errors.New(fmt.Sprintf("Помилка отримання [%s]: %s", file.Name, e))
      }

      buf, _ := ioutil.ReadAll(r)
      if err = ioutil.WriteFile(filepath.Join(a.config.DataDir, file.Name), buf, 0777); err != nil {
        return errors.New(fmt.Sprintf("Помилка при збереженні даних в [%s]: %s", file.Name, err))
      } else {
        a.Log(fmt.Sprintf("Збережено в [%s] (%d байт).", filepath.Join(a.config.DataDir, file.Name), len(buf)))
      }

      e = r.Close()
      if e != nil {
        return errors.New(fmt.Sprintf("Помилка закриття з'єднання [%s]: %s", file.Name, e))
      }
    }

    // Видалення файлів
    if a.config.FtpConfig.DelAfterReceive {
      a.Log("Видалення:")

      for _, file := range a.entries {
        if e := con.Delete(file.Name); e != nil {
          a.Log(fmt.Sprintf("Помилка при видаленні файлу [%s]: %s", file.Name, err))
        } else {
          a.Log(fmt.Sprintf("Файл [%s] видалений з сервера.", file.Name))
        }
      }
    }
  } else {
    return errors.New("жодних файлів до завантаження")
  }

  return nil
}

func (a *App) Unpack() error {
  if len(a.entries) > 0 {
    for i, entry := range a.entries {
      if !entry.IsArchive {
        continue
      }

      src := filepath.Join(a.config.DataDir, entry.Name)
      dst := filepath.Join(a.config.DataDir, strings.Replace(entry.Name, ".", "_", -1)) + "\\"

      if a.config.ArchiverConfig.UseInternal {
        if e := arc.Unarchive(src, dst); e != nil {
          a.Log(e.Error())
        } else {
          a.Log(fmt.Sprintf(`Архів [%s] успішно розпакований в "%s".`, entry.Name, dst))
          a.entries[i].ArcDir = dst
          if a.config.ArchiverConfig.DeleteAfterUnpack {
            _ = os.Remove(src)
          }
        }
      } else {
        args := strings.Split(a.config.ArchiverConfig.Args, " ")
        for index := range args {
          args[index] = strings.Replace(args[index], "%archive_name%", src, -1)
          args[index] = strings.Replace(args[index], "%directory%", dst, -1)
        }

        cmd := exec.Command(a.config.ArchiverConfig.Path, args...)
        if e := cmd.Run(); e != nil {
          a.Log(fmt.Sprintf("Помилка розпаковування файлу [%s]: %s", entry.Name, e.Error()))
        } else {
          a.Log(fmt.Sprintf(`Архів [%s] успішно розпакований в "%s".`, entry.Name, dst))
          a.entries[i].ArcDir = dst

          if a.config.ArchiverConfig.DeleteAfterUnpack {
            _ = os.Remove(src)
          }
        }
      }
    }
  }

  return nil
}

func (a *App) MakeFilesList() error {
  // ручний імпорт
  if a.IsManual() {
    a.Log("Ручний імпорт даних...")
    a.Log(fmt.Sprintf("Файл: [%s]", a.config.ManualFiles))

    b, err := c.LoadFromFile(a.config.ManualFiles)
    if err != nil {
      return err
    }

    if e := json.Unmarshal(b, &a.files); e != nil {
      return err
    }

    for i := range a.files {
      a.files[i].HasError = !c.FileExists(a.files[i].Name)
      if a.files[i].HasError {
        a.files[i].Error = "Файл не знайдено."
      }
    }
  } else {
    index := 0
    for _, entry := range a.entries {
      if entry.IsArchive {
        // архів
        err := filepath.Walk(entry.ArcDir, func(path string, info os.FileInfo, err error) error {
          if c.IsIncluded(a.config.Project.ExtList, strings.ToLower(filepath.Ext(path))) {
            a.files = append(a.files, c.File{Id: index + 1, Name: path, Size: uint64(info.Size())})
            index++
          }

          return nil
        })

        if err != nil {
          return errors.New(fmt.Sprintf("Помилка Walk: %s", err))
        }
      } else {
        // файл
        file := filepath.Join(a.config.DataDir, entry.Name)
        info, err := os.Stat(file)
        if err != nil {
          return errors.New(fmt.Sprintf("Помилка Stat: %s", err))
        }

        if c.IsIncluded(a.config.Project.ExtList, strings.ToLower(filepath.Ext(file))) {
          a.files = append(a.files, c.File{Id: index + 1, Name: file, Size: uint64(info.Size())})
          index++
        }
      }
    }
    a.Log(fmt.Sprintf("Проектних файлів: %d.", len(a.files)))

    // Збереження переліку файлів
    files := append([]c.File{}, a.files...)
    f := filepath.Join(a.config.DataDir, "..", "files.json")
    b, err := json.MarshalIndent(files, "", "  ")
    if err != nil {
      return err
    }
    if e := c.SaveToFile(&b, f); e != nil {
      return e
    }

    s := fmt.Sprintf("@echo off\npushd ..\npushd ..\n%s -files \"%s\"", os.Args[0], f)
    b = []byte{}
    b = append([]byte{}, []byte(s)...)
    if e := c.SaveToFile(&b, filepath.Join(a.config.DataDir, "..", "run.bat")); e != nil {
      return e
    }
  }

  return nil
}

func (a *App) sendMail() error {
  a.composeFiles()

  var ef string
  for _, file := range a.files {
    if file.HasError {
      ef += fmt.Sprintf("  \"%s\": \"%s\"\n", file.Name, file.Error)
    }
  }

  ef = c.IifString(ef != "", "\nПомилкові файли:\n"+ef, "")
  if ef != "" {
    a.Log(ef)
  }

  if len(a.mails) > 0 {
    a.Log(fmt.Sprintf("Сформовано %d лист(и,ів)...", len(a.mails)))

    for i, mail := range a.mails {
      a.Log(fmt.Sprintf("  #%d: %d файл(и,ів) (%d байт)", i+1, len(mail.AttachmentsIdx), mail.AttachmentSize))
    }

    a.Log("Відправлення листів:")

    for mi := range a.mails {
      s := c.IifString(len(a.mails) > 1, fmt.Sprintf(" (лист %d з %d)", mi+1, len(a.mails)), "")
      a.mails[mi].Subject = fmt.Sprintf("%s (%s)%s", a.config.Project.Name, a.config.Project.Htag, s)
      a.mails[mi].Body = c.IifString(a.config.SmtpConfig.NeedToZip, fmt.Sprintf("Вміст архіву: %d файл(и,ів).\n\n", len(a.mails[mi].AttachmentsIdx)), "")
    }

    var workersCount = c.IifInt(len(a.mails) < 4, len(a.mails), 4)
    var mu sync.Mutex
    var wg sync.WaitGroup
    cm := make(chan *c.Mail, workersCount)

    for i := 0; i < workersCount; i++ {
      wg.Add(1)

      go func(in <-chan *c.Mail) {
        defer wg.Done()
        files := make([]string, 0)

        for {
          mail, ok := <-in
          if !ok {
            break
          }

          files = []string{}

          for idx := range mail.AttachmentsIdx {
            files = append(files, a.files[mail.AttachmentsIdx[idx]].Name)
            mu.Lock()
            mail.Body += fmt.Sprintf("%s\n", filepath.Base(a.files[mail.AttachmentsIdx[idx]].Name))
            mu.Unlock()
          }

          a.Log(fmt.Sprintf("  \"%s\" (%d файл(и,ів))...", mail.Subject, len(mail.AttachmentsIdx)))
          if err := a.sendMailEx(mail.Subject, mail.Body, a.config.Project.MailTo, []string{}, []string{}, files, a.config.SmtpConfig.NeedToZip); err != nil {
            a.Log(fmt.Sprintf("Помилка відправлення на пошту: %s.", err.Error()))
            mu.Lock()
            mail.Error = err.Error()
            mu.Unlock()
          } else {
            mu.Lock()
            mail.IsSent = true
            mu.Unlock()
            a.Log(fmt.Sprintf("  \"%s\" (%d файл(и,ів))... Ok", mail.Subject, len(mail.AttachmentsIdx)))
          }
        }
      }(cm)
    }

    // запуск всіх процесів
    a.Log(fmt.Sprintf("Кількість потоків для обробки: %d", workersCount))
    for i := range a.mails {
      cm <- &a.mails[i]
    }
    close(cm)
    wg.Wait()
  }

  return nil
}

func (a *App) SendLogs() error {
  if len(a.config.Project.LogTo) == 0 {
    return nil
  }

  var e error = nil
  var subject = fmt.Sprintf("Протокол роботи по \"%s\" (%s)", a.config.Project.Name, a.config.Project.Htag)
  var body string

  if a.IsManual() {
    body += "Ручний імпорт даних...\n"
  }

  if a.config.Project.UseMail {
    body += fmt.Sprintf("Кількість отриманих файлів(архівів): %d\n", len(a.entries))
    body += fmt.Sprintf("Кількість проектних файлів: %d\n", len(a.files))
    body += "Відправленя листів: \n"
    for i := range a.mails {
      body += fmt.Sprintf("  #%d: \"%s\" (%d файл(и,ів)) [%s]\n",
        i+1,
        a.mails[i].Subject,
        len(a.mails[i].AttachmentsIdx),
        c.IifString(a.mails[i].IsSent, "Ok", a.mails[i].Error),
      )
    }

    var ef string
    for _, file := range a.files {
      if file.HasError {
        ef += fmt.Sprintf("  \"%s\": \"%s\"\n", file.Name, file.Error)
      }
    }

    body += c.IifString(ef != "", "Помилкові файли:\n"+ef, "")
  } else {
    var errFiles int
    var b string
    body += fmt.Sprintf("Кількість проектних файлів: %d\n", len(a.files))
    for _, file := range a.files {
      if file.HasError {
        errFiles++
        b += fmt.Sprintf("  \"%s\":%s %s\n", filepath.Base(file.Name),
          c.IifString(file.Status != 0, "["+strconv.Itoa(file.Status)+"]", ""), file.Error)
      }
    }

    if b != "" {
      body += fmt.Sprintf("Кількість помилкових файлів: %d\n", errFiles)
      body += b
    }
  }

  if e = a.sendMailEx(subject, body,
    a.config.Project.LogTo, a.config.Project.LogCC, a.config.Project.LogBCC,
    []string{a.config.FileLogName}, false); e != nil {
    a.Log(fmt.Sprintf("Помилка відправлення протоколу роботи: '%s'", e.Error()))
  }

  a.Log("Обробка закінчена!\n")

  return e
}

func (a *App) composeFiles() {
  var attachSize uint64 = 0
  var mail = c.Mail{AttachmentsIdx: []int{}}
  var maxAttachSize = uint64(a.config.SmtpConfig.MaxAttachmentSizeMB * c.MB)

  a.mails = make([]c.Mail, 0, len(a.files))

  for index, file := range a.files {
    if !file.HasError {
      attachSize += file.Size
      mail.AttachmentsIdx = append(mail.AttachmentsIdx, index)
      mail.AttachmentSize = attachSize

      if (attachSize >= maxAttachSize) || (index == (len(a.files) - 1)) {
        a.mails = append(a.mails, mail)
        attachSize = 0
        mail = c.Mail{Subject: "", AttachmentsIdx: []int{}, AttachmentSize: 0}
      }
    }
  }
}

func (a *App) makeArchive(files []string, archPath string) error {
  if len(files) == 0 {
    return errors.New("жодних файлів для архівації")
  }

  z := arc.Zip{
    CompressionLevel:       flate.BestCompression,
    MkdirAll:               true,
    SelectiveCompression:   false,
    ContinueOnError:        false,
    OverwriteExisting:      true,
    ImplicitTopLevelFolder: false,
  }
  defer func() {
    e := z.Close()
    if e != nil {
      a.Log(fmt.Sprintf("Помилка закриття внутрішнього архіватора: %s.", e.Error()))
    }
  }()

  if e := z.Archive(files, archPath); e != nil {
    a.Log(fmt.Sprintf("Помилка архівації: %s.", e.Error()))
    return e
  }

  return nil
}

func (a *App) sendMailEx(subject, body string, to []string, cc []string, bcc []string, files []string, archive bool) error {
  m := email.NewEmail()
  m.From = a.config.SmtpConfig.From
  m.To = to
  m.Cc = cc
  m.Bcc = bcc
  m.Subject = subject
  m.Text = []byte(body)

  // append attachments
  if len(files) > 0 {
    // archive
    if archive {
      uuid, err := c.MakeFakeUUID()
      if err != nil {
        return err
      }

      archPath := filepath.Join(a.config.DataDir, fmt.Sprintf("archive_%s (%s).zip", time.Now().Format("150405.000000"), uuid))

      if err := a.makeArchive(files, archPath); err != nil {
        return err
      }

      // append attachments
      att, e := m.AttachFile(archPath)
      if e != nil {
        return e
      }
      f := mime.QEncoding.Encode("utf-8", filepath.Base("archive.zip"))
      att.Header.Set("Content-Disposition", fmt.Sprintf("attachment;\r\n filename=\"%s\"", f))
      att.Header.Set("Content-ID", fmt.Sprintf("<%s>", f))

      _ = os.Remove(archPath)
    } else {
      for _, file := range files {
        att, err := m.AttachFile(file)
        if err != nil {
          return err
        }
        f := mime.QEncoding.Encode("utf-8", filepath.Base(file))
        att.Header.Set("Content-Disposition", fmt.Sprintf("attachment;\r\n filename=\"%s\"", f))
        att.Header.Set("Content-ID", fmt.Sprintf("<%s>", f))
      }
    }
  }

  // mail sending
  auth := smtp.PlainAuth("", a.config.SmtpConfig.Login, a.config.SmtpConfig.Password, a.config.SmtpConfig.Server)
  tlsConfig := &tls.Config{InsecureSkipVerify: true, ServerName: a.config.SmtpConfig.Server}
  // todo перевірка роботи з портом 25
  if e := m.SendWithTLS(a.config.SmtpConfig.Addr(), auth, tlsConfig); e != nil {
    return e
  }

  return nil
}

func (a *App) ParseAndPost() error {
  if a.config.Project.UseMail {
    return a.sendMail()
  } else {
    return a.config.Skynet.ParseAndPost(a.files)
  }
}
