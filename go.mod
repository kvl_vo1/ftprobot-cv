module ftprobot-cv

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/frankban/quicktest v1.6.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/jlaffaye/ftp v0.0.0-20191025175106-a59fe673c9b2
	github.com/jordan-wright/email v0.0.0-20190819015918-041e0cec78b0
	github.com/mholt/archiver v3.1.1+incompatible
	github.com/nwaples/rardecode v1.0.0 // indirect
	github.com/pierrec/lz4 v2.3.0+incompatible // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	golang.org/x/text v0.3.2
)
