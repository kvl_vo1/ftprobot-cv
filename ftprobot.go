package main

import (
  . "ftprobot-cv/app"
)

func main() {
  var a App

  if err := a.Init(); err != nil {
    a.Log(err.Error())
    panic(err)
  }

  if !a.IsManual() {
    if err := a.LoadFromFTP(); err != nil {
      a.Log(err.Error())
      panic(err)
    }

    if err := a.Unpack(); err != nil {
      a.Log(err.Error())
      panic(err)
    }
  }

  if err := a.MakeFilesList(); err != nil {
    a.Log(err.Error())
    panic(err)
  }

  if err := a.ParseAndPost(); err != nil {
    a.Log(err.Error())
    panic(err)
  }

  if err := a.SendLogs(); err != nil {
    a.Log(err.Error())
    panic(err)
  }
}
